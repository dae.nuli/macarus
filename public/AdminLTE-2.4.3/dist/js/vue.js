var app = new Vue({
  el: '#app-order',
  data() {
    return {
      items: [],
      filter: '',
      itemOrder: [],
      payment: 0,
      active: true,
    }
  },
  ready: function(){
    this.fetchItems();
  },
  filters: {
    currency(amount) {
      // let val = (value/1).toFixed(2).replace('.', ',')
      return 'Rp '+amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      // return 'Rp '+numeral(amount).format('0,0');
    }  
  },
  computed: {
    // getItems() {
    //   var items = this.items.filter((item) => {
    //     return item.name.toLowerCase().includes(this.filter.toLowerCase());
    //   });
    // }
    newItem: function() {
      // return this.items;
      return this.items.filter((item) => {
        return item.name.toLowerCase().includes(this.filter.toLowerCase());
      })
    },
    total: function () {
      return this.itemOrder.reduce(
        (acc, item) => acc + item.price*item.qty, 0
      )
    },
    amount: function () {
      let total = 0;
      this.itemOrder.forEach(element => {
        total += element.price*element.qty
      });
      return (this.payment >= total) ? this.payment - total : 0;
      // alert(total);
    }
    // reversedMessage: function () {
    //   // `this` points to the vm instance
    //   return this.item.name.split('').reverse().join('')
    // }
  },
  mounted() {
    axios.get('/api/item').then(response => (this.items = response.data))
  }, 
  methods: {
    additem: function(item) {
      if (!this.contains(this.itemOrder, item.name)) {
        // this.items.splice(index, 1);
        this.itemOrder.push({
          name:item.name,
          price:item.price,
          qty:1,
        });
      } else {
        // return alert(item.name+' has been selected');
        
      }
    },
    removeItem: function(index) {
      return this.itemOrder.splice(index, 1);
      // this.items.push(item);
    },
    contains: function(arr, val) {
      for (var i = 0; i < arr.length; i++) {
          if (arr[i].name === val) {
              return true;
          }
      }
      return false;
    }
  }
});

// Vue.filter('limit', function (value, amount) {
//     return value.filter(function(val, index, arr){
//     return index < amount;      
//     });
// })