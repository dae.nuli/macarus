@extends('admin.layouts.app')

@section('select2-script')
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('AdminLTE-2.4.3/bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('head-script')
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

@endsection

@section('end-script')
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.blockUI.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/bower_components/jquery-form-validator/form-validator/jquery.form-validator.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/jquery.priceformat.min.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/custom.js') }}"></script>
  <script src="{{ asset('AdminLTE-2.4.3/dist/js/vue.js') }}"></script>
  <script type="text/javascript">
    $.validate({
        form : '.form-horizontal',
        onSuccess : function() {
          waiting();
        }
    });
    $('.select2').select2();
    $('.status').wrapAll( "<div class='col-sm-8'></div>");
    $('.status').find('[name="status"]').css('margin-left','0');
    $('.number').priceFormat({
      prefix: '',
      thousandsSeparator: '.',
      clearPrefix:true,
      centsLimit: 0,
      // centsSeparator: '.',
      // clearOnEmpty: true
    });
  </script>
@endsection

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="box" id="app-order">
	<div class="box-header with-border">
        <a href="{{$url}}" class="btn btn-warning"><i class="fa fa-fw fa-arrow-left"></i> Back</a>
	</div>
  <form method="POST" action="{{ $store }}" accept-charset="UTF-8" class="form-horizontal">
    @csrf
    <div class="box-body">
      <div class="row">
        <div class="col-lg-4">
          {{-- <div class="row"> --}}
            <div class="form-group">
              {{-- <label for="name" class="col-sm-2 control-label">Search Item</label> --}}
              <div class="col-sm-12">
                <input class="form-control" v-model="filter" placeholder="Search Item" name="name" type="text" id="name">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-12">
                <table class="table table-hover">
                  <tr v-for="(item, index) in newItem.slice(0,10)" transition="staggered" stagger="100">
                    <td>@{{item.name}}</td>
                    <td>@{{item.price | currency}}</td>
                    <td><button type="button" v-on:click="additem(item)" class="btn btn-xs btn-success pull-right"><i class="fa fa-fw fa-share"></i></button></td>
                  </tr>
                </table>
              </div>
            </div>
          {{-- </div> --}}
        </div>
        <div class="col-lg-8">
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="name" class="col-sm-4 control-label">Table Number</label>
                <div class="col-sm-7">
                  <input class="form-control" data-validation="required" name="name" type="text" id="name">
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Date</label>
                <div class="col-sm-8">
                  <input class="form-control" value="{{date('d F Y, H:i:s')}}" disabled="" disabled="">
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="name" class="col-sm-4 control-label">Employee</label>
                <div class="col-sm-7">
                  <input class="form-control" type="text" value="{{auth()->user()->name}}" disabled="">
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="form-group">
                <label for="name" class="col-sm-3 control-label">Payment</label>
                <div class="col-sm-7">
                  <select class="form-control">
                    @foreach($payment as $row)
                      <option value="{{$row->id}}">{{$row->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Item Name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th>Amount</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody v-if="itemOrder.length">
                <tr v-for="(order, index) in itemOrder">
                  <td>@{{order.name}}</td>
                  <td>@{{order.price | currency}}</td>
                  <td><input type="number" name="qty" v-model.number="order.qty" style="width: 50px"></td>
                  <td>@{{order.price*order.qty | currency}}</td>
                  <td><button type="button" v-on:click="removeItem(index)" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-trash"></i></button></td>
                </tr>
              </tbody>
              <tbody v-else>
                <tr>
                  <td colspan="5" align="center">Order Empty</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="row">
            <div class="col-lg-7">
              <div class="row">
                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Discount</label>
                  <div class="col-sm-7">
                    {{-- <div class="input-group"> --}}
                      {{-- <span class="input-group-addon"><i class="fa fa-money"></i></span> --}}
                      <input type="text" class="form-control" data-validation="required" name="addpayment">
                    {{-- </div> --}}
                    {{-- <input class="form-control" data-validation="required" name="addpayment" type="text"> --}}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Payment Amount</label>
                  <div class="col-sm-7">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-money"></i></span>
                      <input type="text" class="form-control" v-model.number="payment" data-validation="required" name="addpayment">
                    </div>
                    {{-- <input class="form-control" data-validation="required" name="addpayment" type="text"> --}}
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-5">
              <div class="row">
                <div class="form-group">
                  <label class="col-sm-4 control-label">TOTAL</label>
                  <div class="col-sm-6">
                    <label class="control-label">@{{total | currency}}</label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Balance</label>
                  <div class="col-sm-6">
                    <label class="control-label">@{{amount | currency}}</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    <div class="box-footer">
      <div class="col-sm-8 col-sm-offset-2">
          <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </form>
</div>
@endsection