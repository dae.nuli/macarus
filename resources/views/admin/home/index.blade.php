@extends('admin.layouts.app')

@section('content')
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>1</h3>

              <p>Item</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{url('admin/item')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>1</h3>

              <p>Order</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url('admin/order')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>1</h3>

              <p>Customers</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{url('admin/customer')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>-</h3>

              <p>Report</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="{{url('admin/report')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        {{-- <section class="col-lg-6">
         <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Kegiatan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Keterangan</th>
                  <th style="width: 40px">Total</th>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>BARU</td>
                  <td><a href="{{url('admin/kegiatan?status=0')}}" class="label bg-red">1</a></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>SEDANG PROSES</td>
                  <td><a href="{{url('admin/kegiatan?status=3')}}" class="label bg-red">1</a></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>DITERIMA</td>
                  <td><a href="{{url('admin/kegiatan?status=1')}}" class="label bg-red">1</a></td>
                </tr>
                <tr>
                  <td>4.</td>
                  <td>DITOLAK</td>
                  <td><a href="{{url('admin/kegiatan?status=2')}}" class="label bg-red">1</a></td>
                </tr>
                <tr>
                  <td>5.</td>
                  <td>SELESAI</td>
                  <td><a href="{{url('admin/kegiatan?status=4')}}" class="label bg-red">1</a></td>
                </tr>
              </table>
            </div>
          </div>
        </section> --}}
        <section class="col-lg-6">
         {{-- <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Instansi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Keterangan</th>
                  <th style="width: 40px">Total</th>
                </tr>
                <tr>
                  <td>1.</td>
                  <td>BARU</td>
                  <td><a href="{{url('admin/users?status=0')}}" class="label bg-red">1</a></td>
                </tr>
                <tr>
                  <td>2.</td>
                  <td>AKTIF</td>
                  <td><a href="{{url('admin/users?status=1')}}" class="label bg-red">1</a></td>
                </tr>
                <tr>
                  <td>3.</td>
                  <td>TIDAK AKTIF</td>
                  <td><a href="{{url('admin/users?status=2')}}" class="label bg-red">1</a></td>
                </tr>
              </table>
            </div>
          </div> --}}
        </section>
      </div>
@endsection