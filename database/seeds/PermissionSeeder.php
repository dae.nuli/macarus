<?php

use Illuminate\Database\Seeder;
use App\Model\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Permission::truncate();
        Permission::insert([        	
        	[
	        	'name' => 'item_index',
				'description' => 'Menu Index Item'
        	],
        	[
	        	'name' => 'item_create',
				'description' => 'Menu Create Item'
        	],
        	[
	        	'name' => 'item_edit',
				'description' => 'Menu Edit Item'
        	],
            [
                'name' => 'item_detail',
                'description' => 'Menu Detail Item'
            ],
            [
                'name' => 'item_delete',
                'description' => 'Menu Delete Item'
            ],

            [
                'name' => 'item_category_index',
                'description' => 'Menu Index Item Category'
            ],
            [
                'name' => 'item_category_create',
                'description' => 'Menu Create Item Category'
            ],
            [
                'name' => 'item_category_edit',
                'description' => 'Menu Edit Item Category'
            ],
            [
                'name' => 'item_category_detail',
                'description' => 'Menu Detail Item Category'
            ],
            [
                'name' => 'item_category_delete',
                'description' => 'Menu Delete Item Category'
            ],
        	
        	[
	        	'name' => 'order_index',
				'description' => 'Menu Index Order'
        	],
        	[
	        	'name' => 'order_create',
				'description' => 'Menu Create Order'
        	],
        	[
	        	'name' => 'order_edit',
				'description' => 'Menu Edit Order'
        	],
        	[
	        	'name' => 'order_detail',
				'description' => 'Menu Detail Order'
        	],
            [
                'name' => 'order_delete',
                'description' => 'Menu Delete Order'
            ],
        	
        	[
	        	'name' => 'report_index',
				'description' => 'Menu Index Report'
        	],
        	[
	        	'name' => 'report_create',
				'description' => 'Menu Create Report'
        	],
        	[
	        	'name' => 'report_edit',
				'description' => 'Menu Edit Report'
        	],
        	[
	        	'name' => 'report_detail',
				'description' => 'Menu Detail Report'
        	],
            [
                'name' => 'report_delete',
                'description' => 'Menu Delete Report'
            ],
        	
        	[
	        	'name' => 'customer_index',
				'description' => 'Menu Index Customers'
        	],
        	[
	        	'name' => 'customer_create',
				'description' => 'Menu Create Customers'
        	],
        	[
	        	'name' => 'customer_edit',
				'description' => 'Menu Edit Customers'
        	],
        	[
	        	'name' => 'customer_detail',
				'description' => 'Menu Detail Customers'
        	],
            [
                'name' => 'customer_delete',
                'description' => 'Menu Delete Customers'
            ],
            
            [
                'name' => 'visitor_index',
                'description' => 'Menu Index Visitor'
            ],
            
            [
                'name' => 'admin_index',
                'description' => 'Menu Index Admin'
            ],
            [
                'name' => 'admin_create',
                'description' => 'Menu Create Admin'
            ],
            [
                'name' => 'admin_edit',
                'description' => 'Menu Edit Admin'
            ],
            [
                'name' => 'admin_delete',
                'description' => 'Menu Delete Admin'
            ],
            
            [
                'name' => 'role_index',
                'description' => 'Menu Index Role'
            ],
            [
                'name' => 'role_create',
                'description' => 'Menu Create Role'
            ],
            [
                'name' => 'role_edit',
                'description' => 'Menu Edit Role'
            ],
            [
                'name' => 'role_delete',
                'description' => 'Menu Delete Role'
            ],
            
            [
                'name' => 'permission_index',
                'description' => 'Menu Index Permission'
            ],
            
            [
                'name' => 'contact_index',
                'description' => 'Menu Index Contact'
            ]
        ]);
    }
}
