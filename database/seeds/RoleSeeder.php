<?php

use Illuminate\Database\Seeder;
use App\Model\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Role::truncate();
        Role::insert([
        	[
                'id' => 1,
                'slug' => 'administrator',
	        	'name' => 'Administrator',
				'description' => 'Super User',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	],
        	[
                'id' => 2,
                'slug' => 'owner',
	        	'name' => 'Owner',
				'description' => 'Owner User',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	],
        	[
                'id' => 3,
                'slug' => 'cashier',
	        	'name' => 'Cashier',
				'description' => 'Cashier User',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	],
        	[
                'id' => 4,
                'slug' => 'chef',
	        	'name' => 'Chef',
				'description' => 'Chef User',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
        	]
        ]);
    }
}
