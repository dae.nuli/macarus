<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Model\Role;
use App\Model\UserRole;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$ad = User::find(1);
    	$rl = Role::where('slug', 'administrator')->first();
    	UserRole::truncate();
        UserRole::insert([
            'user_id' => $ad->id,
            'role_id' => $rl->id,
        ]);
    }
}
