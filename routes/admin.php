<?php

Route::get('/home', 'Admin\HomeController@index')->name('home');

Route::get('category/data', 'Admin\ItemCategoryController@data')->name('category.data');
Route::resource('category', 'Admin\ItemCategoryController');

Route::get('item/data', 'Admin\ItemController@data')->name('item.data');
Route::resource('item', 'Admin\ItemController');

Route::get('order/data', 'Admin\OrderController@data')->name('order.data');
Route::resource('order', 'Admin\OrderController');
