<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use App\Model\ItemCategory;

class ItemForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('category_id', 'select', [
                'choices' => ItemCategory::pluck('name', 'id')->toArray(),
                // 'empty_value' => '- Please Select -',
                'label' => 'Category',
                'attr' => [
                    'data-validation' => 'required',
                    'class' => 'select2 form-control',
                    // 'multiple' => 'multiple'
                ]
            ])
            ->add('name', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('price', 'text', [
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'number form-control'
                ]
            ])
            ->add('description', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('status', 'choice', [
                'choices' => [1 => 'ACTIVE', 0 => 'NOT ACTIVE'],
                'choice_options' => [
                    'wrapper' => ['class' => 'radio status'],
                    'label_attr' => ['class' => ''],
                ],
                'attr' => ['data-validation' => 'required'],
                'selected' => [1],
                'expanded' => true,
                'multiple' => false
            ]);
    }
}
