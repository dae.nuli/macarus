<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class CustomerForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('email', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('phone', 'text', [
                'attr' => ['data-validation' => 'required']
            ])
            ->add('birth_date', 'text', [
                'value' => date('Y-m-d'),
                'attr' => [
                	'data-validation' => 'required',
                    'class' => 'datepicker form-control'
                ]
            ]);
    }
}
