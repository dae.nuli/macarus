<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Items;

class ItemController extends Controller
{
	public function __construct(Items $table) 
    {
        $this->table = $table;
    }

    public function index()
    {
    	$data = $this->table->orderBy('name', 'asc')->get();
    	return response()->json($data);
    }
}
