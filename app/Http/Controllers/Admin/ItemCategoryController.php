<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilder;
use App\Model\ItemCategory;
use DataTables;
use Form;

class ItemCategoryController extends Controller
{
    private $folder = 'admin.category';
    private $uri = 'admin.category';
    private $title = 'Item Category';
    private $desc = 'Description';

    public function __construct(ItemCategory $table) 
    {
        $this->middleware('permission:item_category_index', ['only' => ['index','data']]);
        $this->middleware('permission:item_category_create', ['only' => ['create','store']]);
        $this->middleware('permission:item_category_edit', ['only' => ['edit','update']]);
        $this->middleware('permission:item_category_delete', ['only' => ['destroy']]);
        $this->table = $table;
    }

    public function index(Request $request)
    {
        $data['title'] = $this->title;
        $data['ajax'] = route($this->uri.'.data');
        $data['create'] = route($this->uri.'.create');
        return view($this->folder.'.index',$data);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->table->select(['id', 'name', 'created_at']);
            return DataTables::of($data)
            // ->editColumn('id','<input type="checkbox" class="checkbox" name="id[]" value="{{$id}}"/>')
                
                ->addColumn('action', function ($index) {
                    $tag = Form::open(array("url" => route($this->uri.'.destroy',$index->id), "method" => "DELETE"));
                    $tag .= (auth()->user()->can('item_category_edit')) ? "<a href=".route($this->uri.'.edit',$index->id)." class='btn btn-primary btn-xs'>EDIT</a>" : '';
                    // $tag .= " <a href=".route($this->uri.'.show',$index->id)." class='btn btn-success btn-xs'>Show</a>";
                    $tag .= (auth()->user()->can('item_category_delete')) ? " <button type='submit' class='delete btn btn-danger btn-xs'>Delete</button>" : '';
                    $tag .= Form::close();
                    return $tag;
                })
                ->rawColumns(['id', 'action'])
                ->make(true);
        }
    }

    public function create(FormBuilder $formBuilder)
    {
        $data['title'] = $this->title;
        $data['form'] = $formBuilder->create('App\Forms\ItemCategoryForm', [
            'method' => 'POST',
            'url' => route($this->uri.'.store')
        ]);
        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function edit(FormBuilder $formBuilder, $id)
    {
        $data['title'] = $this->title;
        $tbl = $this->table->find($id);
        $data['form'] = $formBuilder->create('App\Forms\ItemCategoryForm', [
            'method' => 'PUT',
            'model' => $tbl,
            'url' => route($this->uri.'.update', $id)
        ]);

        $data['url'] = route($this->uri.'.index');
        return view($this->folder.'.create', $data);
    }

    public function store(Request $request)
    {
        $this->table->create($request->all());
        return redirect(route($this->uri.'.index'))->with('success',trans('message.create'));
    }

    public function update(Request $request, $id)
    {
        $this->table->findOrFail($id)->update($request->all());
        return redirect(route($this->uri.'.index'))->with('success', trans('message.update'));
    }
    
    public function destroy($id)
    {
        $tb = $this->table->findOrFail($id);
        $tb->delete();
        return response()->json(['msg' => true,'success' => trans('message.delete')]);
    }
}
