<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SaleItem extends Model
{
    use SoftDeletes;

    protected $table = 'sale_item';
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'sale_id', 'item_id', 'name', 'price', 'quantity', 'total_price',
    ];
}