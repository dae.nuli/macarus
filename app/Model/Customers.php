<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customers extends Model
{

	use SoftDeletes;

    protected $table = 'customers';
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name', 'email', 'phone', 'birth_date',
    ];

}