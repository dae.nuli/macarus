<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Permission;

class Role extends Model
{
    protected $table = 'role';
    
    protected $fillable = [
        'slug', 'name', 'description',
    ];

    public function cachedPermissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permission', 'role_id', 'permission_id');
    }
}