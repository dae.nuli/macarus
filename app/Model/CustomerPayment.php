<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerPayment extends Model
{
	use SoftDeletes;

    protected $table = 'customer_payment';
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'sale_id', 'payment_method_id', 'amount', 'comment',
    ];

}