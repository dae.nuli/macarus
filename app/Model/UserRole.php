<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Role;

class UserRole extends Model
{
	use SoftDeletes;

    protected $table = 'user_role';
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'user_id', 'role_id',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
}