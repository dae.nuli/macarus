<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\ItemCategory;

class Items extends Model
{

	use SoftDeletes;

    protected $table = 'items';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'category_id', 'deleted_at', 'updated_at',
    ];
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'category_id', 'name', 'price', 'status', 'description',
    ];

    public function setPriceAttribute($val)
    {
        $this->attributes['price'] = str_replace('.','',$val);
    }

    public function category()
    {
        return $this->belongsTo(ItemCategory::class);
    }

}