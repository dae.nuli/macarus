<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sales extends Model
{

	use SoftDeletes;

    protected $table = 'sales';
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'table_number', 'customer_id', 'user_id', 'total', 'status', 'comment',
    ];
}