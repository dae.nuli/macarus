<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permission';
    
    protected $dates = ['deleted_at'];
    
    protected $fillable = [
        'name', 'description',
    ];
}